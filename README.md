# Political ideas

Alex Bezuska - Louisville Kentucky


### Environmental
- Implement a progressive carbon tax on businesses and households such that the tax is some amount per tonne of greenhouse gas emissions, adjusted by the annual business income or household income.
- Fully fund the complete rollout of non-battery electric passenger rail at all across the entire country, connecting all major cities.
- End all subsidies to oil and gas companies.
- Ban the private ownership of large planes.
- Fully fund conversion to solar and nuclear power.
- Fully fund nuclear fusion research and development.
- Trees are not allowed to be cut down unless they threaten a pre-existing structure or are sick or non-native.
- Ban gas-powered leaf blowers.
- End importing of foriegn oil
- Protect national parks, reservation, and areas near residencies from fraking, drilling, or pipeline creation
- Perminintly protect national parks, reservation lands from future development from outside parties
- Clean water for all by updating aging pipes nationwide
- Replace all coal production with clean energy
    - Aid and training for coal workers to transition into clean energy jobs
- Establish / increase emissions tax on companies



### Criminal Justice Reform
- Abolish the death penalty
- Mandatory free weekly therapy sessions with licensed therapists for all jail and prison inmates
- Ban prison labor for companies
- Ban all firearms that aren't legitimately designed for and used for hunting, including for police, government agencies
- Free Therapy by licensed therapist for all veterans
- Instead of deportations for illegal immigrants, they will have the choice to return to their country or be given citizenship so they work over-the-table jobs and pay taxes but also receive all the benefits associated with being a citizen
- All Police must wear and have bodycams turned on at all times
    - Add access to these videos to Freedom of Information act 


### Economic Policies and Welfare
- Abolish tipping
- End government funding for private education institutions like private schools
- End tax exempt status for religious organizations
- Tax anyone who makes over 2 million per year at a 50% tax bracket
- End tax loopholes for companies
- Tax anyone who has a net worth over 1 billion at 75% of their annual income each year
- Universal single-payer healthcare including medical, dental, and vision
    - Free prescription drugs, pharmaceutical companies cannot charge the government (single-payer) more than $50 per person per month, per prescription
    - Including gender-affirming procedures and care
    - Including abortion care in first month of pregnancy (need to research this more)
    - Mandatory male birth control from age 18 until consenting other party agrees in writing to get pregnant
    - Free contraception and free morning after pill, no questions asked
- Disallow citizens from suing for medical malpractice while also funding a non-profit group which sues the government on behalf of any citizen who files a malpractice claim
- Double tariffs on all foreign-made products to force US companies to build manufacturing in the US and provide more jobs for US citizens
- Universal basic income of $1000 per person over 18 years old
- Federal minimum wage of $15/hour
- Renter protection from slum lords where tenants can report landlords who don't provide necessary basic repairs and level of safety and cleanliness
- Establish minimum distance between "dollar stores"
- Ban cash advance loans
- Inforce all employers to provide paid days off for election days
- Inforce all employers to provide paid 4 month new parent leave for birth or adoption, for both parents
- Government matches contributions all private contributions to ACLU, and Doctors Without Borders
- Abolish privately or foriegn owned toll bridges


### Agreculture
- Increase subsidies to family-owned farms that demonstrate strict and compentent adherence to regenerative agricultural practices.
- Scale back corn subsidies dramatically.
- Farm animals must be given adequate space and outdoor time each day
- Farm animals can be given antibiotics but only in cases to treat medical issues
- Farm animals can only be fed up to 50% corn diet
- Ban use of growth hormones
- Ban creation of seedless dead-end varieties of plants
- Allow farmers to save thier seeds
- Ban use of Roundup and similar harmful pesticides


### Right to Repair
 - Consumers have the right to repair their own products, including but not limited to electronic devices, agricultural equipment, and medical devices, without voiding warranties or facing unreasonable barriers from manufacturers.
 - Manufacturers must provide consumers and independent repair providers with access to necessary documentation, tools, parts, and software required for diagnosis, maintenance, and repair of products. This includes making these resources available at fair and reasonable terms
 - Manufacturers are required to provide support for a reasonable duration after the product's sale


### Social
- all restrooms must be gender neutral and all toilets and urinals must be enclosed with locking stalls
- End all trans bathroom bans 


### Military and Foreign Policy
- Reduce all military spending to 1/4 of current spending
- Raise the age for military service to 25
- Ban military recruitment and drafts
- Bring home any US military serving in other countries unless removal would cause major harm
- End all US government sales or aid involving weapons for foreign countries
_ Ban weapons sales from private companies to foriegn countries
- End all monetary aid for foreign countries, only food or medical supplies
- Funding of non profit whistleblower protection and free legal support


### Health and Welfare
- Ban use of High fructose corn syrup and artificial coloring in foods
- Tax foods deemed unhealthy matching food ingredients deemed to cause harm to humans by current EU standards
- Ban private health insurance
- $30 tax per person per car per month which covers basic liability car insurance, citizens can opt out and go with a private car insurance provider for coverage above liability


### Electoral Reforms
- Add Puerto Rico, American Samoa, and Washington DC as official states
- Automatic voter registration at age 18
- Ranked choice voting for all elections nationwide
- End primaries
- No party affiliation required for voter registration
- Ban private tax preparation including apps like TurboTax
- Taxes done by IRS and citizens' returns or payments are automatically calculated
- Enforce 4-year term limits for all political offices
- Abolish lobbying
- Abolish private companies funding politicians, causes, and parties
- Abolish PACs and Super PACs


### Education
- Free school breakfast and lunch available to all K-12 students
- Expand K-12 Art, Music, and STEM programs
- Expand economics, money management, and budgeting education in high school


### Misc
- Nationwide adoption of the Metric system
- End daylight saving time